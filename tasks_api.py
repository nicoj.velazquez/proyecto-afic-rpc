from flask_cors import CORS
from flask import Flask, request, jsonify
from flask_httpauth import HTTPBasicAuth
import json
import os

app = Flask(__name__)
CORS(app)
auth = HTTPBasicAuth()

users = {
    "admin": "123456"
}

@auth.verify_password
def verify_password(username, password):
    if username in users and users[username] == password:
        return username

# Cargamos las tareas desde el archivo al inicio
if os.path.exists("tasks.json"):
    with open("tasks.json", "r") as f:
        tasks = json.load(f)
else:
    tasks = []

# Guardamos las tareas en el archivo
def save_tasks():
    with open("tasks.json", "w") as f:
        json.dump(tasks, f)

@app.route('/tasks', methods=['GET'])
def get_all_tasks():
    return jsonify(tasks), 200

@app.route('/tasks', methods=['POST'])
@auth.login_required
def add_task():
    new_task = {
        'id': len(tasks) + 1,
        'title': request.json['title'],
        'description': request.json.get('description', ""),
        'done': False
    }
    tasks.append(new_task)
    save_tasks()
    return jsonify(new_task), 201

@app.route('/tasks/<int:task_id>', methods=['GET'])
def get_one_task(task_id):
    task = next((task for task in tasks if task['id'] == task_id), None)
    if task:
        return jsonify(task), 200
    else:
        return jsonify({"error": "Task not found"}), 404

@app.route('/tasks/<int:task_id>', methods=['PUT'])
@auth.login_required
def edit_task(task_id):
    task = next((task for task in tasks if task['id'] == task_id), None)
    if task:
        task['title'] = request.json.get('title', task['title'])
        task['description'] = request.json.get('description', task['description'])
        task['done'] = request.json.get('done', task['done'])
        save_tasks()
        return jsonify(task), 200
    else:
        return jsonify({"error": "Task not found"}), 404

@app.route('/tasks/<int:task_id>', methods=['DELETE'])
@auth.login_required
def remove_task(task_id):
    global tasks
    tasks = [task for task in tasks if task['id'] != task_id]
    save_tasks()
    return jsonify({'result': True}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)

