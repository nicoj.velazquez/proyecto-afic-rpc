# Importamos las bibliotecas necesarias.
import requests  # Biblioteca para realizar solicitudes HTTP.
import json  # Biblioteca para trabajar con datos en formato JSON.

# Función para obtener el usuario y contraseña manualmente.
def get_credentials():
    username = input("Ingresa tu usuario: ")
    password = input("Ingresa tu contraseña: ")
    return (username, password)

# Solicitamos las credenciales al usuario.
AUTH = get_credentials()

# Función para mostrar las tareas de manera estructurada y amigable.
def display_tasks(tasks):
    if not tasks:
        print("No hay tareas para mostrar.")
        return

    print("\nTareas:")
    print("───────\n")
    for task in tasks:
        status = "Hecho" if task['done'] else "Pendiente"
        print(f"ID: {task['id']}")
        print(f"Título: {task['title']}")
        print(f"Descripción: {task['description']}")
        print(f"Estado: {status}")
        print("────────────────────────\n")

# Función para obtener todas las tareas del servidor.
def get_tasks():
    response = requests.get('http://10.24.34.185:8080/tasks', auth=AUTH) # 127.0.0.1 ip de la mac, sino, localhost
    if response.status_code == 200:
        return response.json()
    elif response.status_code == 401:
        print("Error de autenticación. Verifica tus credenciales.")
        return None
    else:
        print(f"Error: {response.status_code}")
        return None

# Función para crear una nueva tarea en el servidor.
def create_task(title, description=""):
    payload = {
        'title': title,
        'description': description
    }
    headers = {'Content-Type': 'application/json'}
    response = requests.post('http://10.24.34.185:8080/tasks', data=json.dumps(payload), headers=headers, auth=AUTH)
    return response.json()

# Función para actualizar una tarea existente en el servidor.
def update_task(task_id, title=None, description=None, done=None):
    payload = {}
    if title:
        payload['title'] = title
    if description:
        payload['description'] = description
    if done is not None:
        payload['done'] = done

    headers = {'Content-Type': 'application/json'}
    response = requests.put(f'http://10.24.34.185:8080/tasks/{task_id}', data=json.dumps(payload), headers=headers, auth=AUTH)
    return response.json()

# Función para eliminar una tarea del servidor.
def delete_task(task_id):
    response = requests.delete(f'http://10.24.34.185:8080/tasks/{task_id}', auth=AUTH)
    return response.json()

if __name__ == '__main__':
    while True:  # Bucle principal
        print("\nMenu:")
        print("1. Obtener todas las tareas")
        print("2. Crear una nueva tarea")
        print("3. Actualizar una tarea")
        print("4. Eliminar una tarea")
        print("5. Salir")
        choice = input("\nSeleccione una opción: ")

        if choice == "1":
            tasks = get_tasks()
            display_tasks(tasks)
        elif choice == "2":
            title = input("Introduce el título de la tarea: ")
            description = input("Introduce una descripción (opcional): ")
            task = create_task(title, description)
            print(task)
        elif choice == "3":
            task_id = int(input("Introduce el ID de la tarea a actualizar: "))
            title = input("Introduce el nuevo título (deja en blanco para no cambiarlo): ")
            description = input("Introduce la nueva descripción (deja en blanco para no cambiarlo): ")
            done = input("¿La tarea está terminada? (si/no, deja en blanco para no cambiarlo): ")
            done = True if done.lower() == 'si' else False if done.lower() == 'no' else None
            task = update_task(task_id, title, description, done)
            print(task)
        elif choice == "4":
            task_id = int(input("Introduce el ID de la tarea a eliminar: "))
            result = delete_task(task_id)
            print(result)
        elif choice == "5":
            print("Saliendo del programa...")
            break  # Terminamos el bucle y salimos del programa
        else:
            print("Opción no válida")

